%global app_id de.haeckerfelix.Shortwave

Name:           shortwave
Version:        5.0.0
Release:        1%{?dist}
Summary:        An internet radio player

License:        GPL-3.0-or-later
URL:            https://gitlab.gnome.org/World/Shortwave
Source0:        %{url}/-/archive/%{version}/Shortwave-%{version}.tar.gz

BuildRequires:  cargo-rpm-macros >= 24
BuildRequires:  gcc
BuildRequires:  meson

BuildRequires:  desktop-file-utils
BuildRequires:  git

BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:  pkgconfig(gstreamer-audio-1.0)
BuildRequires:  pkgconfig(gstreamer-plugins-bad-1.0)
BuildRequires:  pkgconfig(gtk4)
BuildRequires:  pkgconfig(lcms2)
BuildRequires:  pkgconfig(libadwaita-1)
BuildRequires:  pkgconfig(libseccomp)
BuildRequires:  pkgconfig(openssl)
BuildRequires:  pkgconfig(shumate-1.0)
BuildRequires:  pkgconfig(sqlite3)

%description
Shortwave is an internet radio player that provides access to a station database
with over 50,000 stations.

%prep
%autosetup -n Shortwave-%{version}

%build
%meson
%meson_build

%install
%meson_install
%find_lang %{name}

%files -f %{name}.lang
%license COPYING.md
%doc README.md
%{_bindir}/%{name}
%{_datadir}/applications/%{app_id}.desktop
%{_datadir}/dbus-1/services/%{app_id}.service
%{_datadir}/glib-2.0/schemas/%{app_id}.gschema.xml
%{_datadir}/icons/hicolor/scalable/apps/%{app_id}.svg
%{_datadir}/icons/hicolor/symbolic/apps/%{app_id}-symbolic.svg
%{_metainfodir}/%{app_id}.metainfo.xml
%{_datadir}/%{name}/%{app_id}.gresource

%changelog
* Sun Feb 9 2025 Umut Demir <umutd3401@posteo.com> - 5.0.0-1
- Update to 5.0.0

* Mon Oct 21 2024 Umut Demir <umutd3401@posteo.com> - 4.0.1-1
- Initial build
