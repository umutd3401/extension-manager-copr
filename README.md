# spec-files
| Package           | f40                | f41                | f42                |
|-------------------|:------------------:|:------------------:|:------------------:|
| citations         | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| extension-manager | :white_check_mark: | :white_check_mark: |                    |
| folio             | :white_check_mark: | :white_check_mark: |                    |
| galaxybudsclient  |                    | :white_check_mark: | :white_check_mark: |
| gdsfactory        | :white_check_mark: |                    |                    |
| halloy            |                    | :white_check_mark: | :white_check_mark: |
| meep              | :white_check_mark: |                    |                    |
| newsflash         | :white_check_mark: | :white_check_mark: |                    |
| papers            |                    | :white_check_mark: |                    |
| planify           | :white_check_mark: | :white_check_mark: | :white_check_mark: |
| shortwave         |                    | :white_check_mark: | :white_check_mark: |
| showtime          |                    | :white_check_mark: |                    |
