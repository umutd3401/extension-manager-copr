Name:           planify
Version:        4.12.0
Release:        1%{?dist}
Summary:        A planner for GNOME

License:        GPL-3.0-only
URL:            https://github.com/alainm23/planify
Source0:        %{url}/archive/refs/tags/%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  meson
BuildRequires:  vala

BuildRequires:  desktop-file-utils
BuildRequires:  gettext

BuildRequires:  pkgconfig(gee-0.8)
BuildRequires:  pkgconfig(granite-7)
BuildRequires:  pkgconfig(gtk4)
BuildRequires:  pkgconfig(gtksourceview-5)
BuildRequires:  pkgconfig(gxml-0.20)
BuildRequires:  pkgconfig(json-glib-1.0)
BuildRequires:  pkgconfig(libadwaita-1)
BuildRequires:  pkgconfig(libecal-2.0)
BuildRequires:  pkgconfig(libportal)
BuildRequires:  pkgconfig(libportal-gtk4)
BuildRequires:  pkgconfig(libsoup-3.0)
BuildRequires:  pkgconfig(webkitgtk-6.0)

%description
Task manager with Todoist & Nextcloud support designed for GNU/Linux.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%prep
%autosetup

%build
%meson -Dprofile="default"
%meson_build

%install
%meson_install
rm %{buildroot}/%{_datadir}/icons/hicolor/scalable/apps/io.github.alainm23.planify.Devel.svg
%find_lang io.github.alainm23.planify

%files -f io.github.alainm23.planify.lang
%license LICENSE
%doc README.md
%{_bindir}/io.github.alainm23.planify
%{_bindir}/io.github.alainm23.planify.quick-add
%{_libdir}/libplanify.so.0
%{_libdir}/libplanify.so.0.1
%{_datadir}/appdata/io.github.alainm23.planify.appdata.xml
%{_datadir}/applications/io.github.alainm23.planify.desktop
%{_datadir}/glib-2.0/schemas/io.github.alainm23.planify.gschema.xml
%{_datadir}/gtksourceview-5/language-specs/markdownpp.lang
%{_datadir}/gtksourceview-5/styles/markdown.xml
%{_datadir}/gtksourceview-5/styles/markdown_dark.xml
%{_datadir}/icons/hicolor/scalable/apps/io.github.alainm23.planify.svg
%{_datadir}/icons/hicolor/symbolic/apps/io.github.alainm23.planify-symbolic.svg
%{_datadir}/vala/vapi/planify.deps
%{_datadir}/vala/vapi/planify.vapi

%files devel
%{_libdir}/libplanify.so
%{_includedir}/planify/planify.h
%{_libdir}/pkgconfig/planify.pc

%changelog
* Sun Feb 9 2025 Umut Demir <umutd3401@posteo.com> - 4.12.0-1
- Update to 4.12.0

* Tue Nov 5 2024 Umut Demir <umutd3401@posteo.com> - 4.11.6-1
- Update to 4.11.6

* Tue Oct 15 2024 Umut Demir <umutd3401@posteo.com> - 4.11.5-1
- Update to 4.11.5

* Thu Sep 19 2024 Umut Demir <umutd3401@posteo.com> - 4.11.4-1
- Update to 4.11.4
- Add description

* Sat Sep 14 2024 Umut Demir <umutd3401@mailbox.org> - 4.11.2-1
- Update to 4.11.2

* Fri Aug 30 2024 Umut Demir <umutd3401@mailbox.org> - 4.11.0-1
- Update to 4.11.0
- Remove devel icon
