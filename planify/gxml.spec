Name:           gxml
Version:        0.20
Release:        1%{?dist}
Summary:        A XML parser and writer

License:        LGPL-2.1
URL:            https://gitlab.gnome.org/GNOME/gxml
Source0:        %{url}/-/archive/%{name}-%{version}/%{name}-%{name}-%{version}.tar.gz

BuildRequires:  meson
BuildRequires:  gcc
BuildRequires:  vala
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(gee-0.8)

%description
GXml provides a GObject API for manipulating XML and a Serializable framework
from GObject to XML.

GXml provide a DOM level 4 API and CSS Selectors Level 3 for XML with multiple
backends.

GXml also provides a powerful XML to/from GObject automatic serialization. It
uses GObject and "The Power of Nick"(C).

%package        devel
Summary:        Development files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.

%global debug_package %{nil}

%prep
%autosetup -n %{name}-%{name}-%{version}

%build
%meson
%meson_build

%install
%meson_install

%files
%license COPYING
%doc AUTHORS NEWS README
%{_libdir}/girepository-1.0/GXml-0.20.typelib
%{_libdir}/libgxml-0.20.so.2
%{_libdir}/libgxml-0.20.so.2.0.2
%{_datadir}/vala/vapi/gxml-0.20.deps
%{_datadir}/vala/vapi/gxml-0.20.vapi

%files devel
%{_libdir}/libgxml-0.20.so
%{_libdir}/pkgconfig/gxml-0.20.pc
%{_datadir}/gir-1.0/GXml-0.20.gir
%{_includedir}/gxml-0.20/gxml/gxml.h

%changelog
%autochangelog
