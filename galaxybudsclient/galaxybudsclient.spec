%global debug_package %{nil}
%global __os_install_post %{nil}

Name:           galaxybudsclient
Version:        5.1.2
Release:        1%{?dist}
Summary:        Unofficial Galaxy Buds Manager for Linux

License:        GPL-3.0-only
URL:            https://github.com/timschneeb/GalaxyBudsClient
Source0:        %{url}/archive/refs/tags/%{version}.tar.gz
Source1:        galaxybudsclient.desktop

BuildRequires:  dotnet-sdk-8.0

%description
Configure and control any Samsung Galaxy Buds device and integrate them into
your desktop.

Aside from standard features known from the official Android app, this project
helps you to release the full potential of your earbuds and implements new
functionality such as:

- Detailed battery statistics
- Diagnostics and factory self-tests
- Loads of hidden debugging information
- Customizable long-press touch actions
- Firmware flashing, downgrading (Buds+, Buds Pro)
and much more...

%prep
%autosetup -n GalaxyBudsClient-%{version}

%build
dotnet restore \
       -r linux-x64 \
       --configfile GalaxyBudsClient/nuget.config \
       GalaxyBudsClient/GalaxyBudsClient.csproj

dotnet publish \
       -r linux-x64 \
       -o bin_linux64 \
       -c Release \
       -p:PublishSingleFile=true \
       --self-contained true \
       --no-restore \
       GalaxyBudsClient/GalaxyBudsClient.csproj

%install
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/144x144/apps/
mkdir -p %{buildroot}%{_datadir}/applications/

install -m 755 bin_linux64/GalaxyBudsClient %{buildroot}%{_bindir}/GalaxyBudsClient
install -m 644 GalaxyBudsClient/Resources/icon.png %{buildroot}%{_datadir}/icons/hicolor/144x144/apps/GalaxyBudsClient.png
install -m 644 %{SOURCE1} %{buildroot}%{_datadir}/applications/GalaxyBudsClient.desktop

%files
%license LICENSE
%doc README.md
%{_bindir}/GalaxyBudsClient
%{_datadir}/icons/hicolor/144x144/apps/GalaxyBudsClient.png
%{_datadir}/applications/GalaxyBudsClient.desktop

%changelog
* Sun Feb 9 2025 Umut Demir <umutd3401@posteo.com> - 5.1.2-1
- Update to 5.1.2

* Sun Jan 5 2025 Umut Demir <umutd3401@posteo.com> - 5.1.1-1
- Update to 5.1.1

* Fri Dec 6 2024 Umut Demir <umutd3401@posteo.com> - 5.1.0-1
- Initial build
