%undefine __brp_mangle_shebangs

Name:           citations
Version:        0.7.0
Release:        1%{?dist}
Summary:        Manage your bibliography

License:        GPL-3.0-or-later
URL:            https://gitlab.gnome.org/World/citations
Source0:        %{url}/-/archive/%{version}/citations-%{version}.tar.gz

BuildRequires:  cargo-rpm-macros
BuildRequires:  meson

BuildRequires:  desktop-file-utils
BuildRequires:  gettext

BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gtk4)
BuildRequires:  pkgconfig(gtksourceview-5)
BuildRequires:  pkgconfig(libadwaita-1)
BuildRequires:  pkgconfig(openssl)
BuildRequires:  pkgconfig(poppler-glib)

%description
Manage your bibliographies using the BibTeX format.

%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install
%find_lang %{name}

%files -f %{name}.lang
%license LICENSE
%doc README.md
%{_bindir}/citations
%{_datadir}/applications/org.gnome.World.Citations.desktop
%{_datadir}/citations/resources.gresource
%{_datadir}/glib-2.0/schemas/org.gnome.World.Citations.gschema.xml
%{_datadir}/icons/hicolor/scalable/apps/org.gnome.World.Citations.svg
%{_datadir}/icons/hicolor/symbolic/apps/org.gnome.World.Citations-symbolic.svg
%{_metainfodir}/org.gnome.World.Citations.metainfo.xml

%changelog
* Tue Sep 24 2024 Umut Demir <umutd3401@posteo.com> - 0.7.0-1
- Update to 0.7.0
